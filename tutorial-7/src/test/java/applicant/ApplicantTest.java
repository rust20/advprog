package applicant;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;

public class ApplicantTest {

    private Applicant applicant;
    private Predicate<Applicant> creditCheck;
    private Predicate<Applicant> employmentCheck;
    private Predicate<Applicant> crimeCheck;
    private Predicate<Applicant> qualifiedCheck;

    @Before
    public void setUp() {
        applicant = new Applicant();
        creditCheck = app -> app.getCreditScore() > 600;
        employmentCheck = app -> app.getEmploymentYears() > 0;
        crimeCheck = app -> app.hasCriminalRecord();
        qualifiedCheck = app -> app.isCredible();
    }

    @Test
    public void testCredit() {
        assertTrue(creditCheck.test(applicant));
    }

    @Test
    public void testEmployment() {
        assertTrue(employmentCheck.test(applicant));
    }

    @Test
    public void testCrime() {
        assertTrue(crimeCheck.test(applicant));
    }

    @Test
    public void testQualified() {
        assertTrue(qualifiedCheck.test(applicant));
    }

    @Test
    public void testEvaluate() {
        Applicant.evaluate(applicant, qualifiedCheck);
    }

    @Test
    public void testprintEvaluate() {
        assertEquals("Result of evaluating applicant : accepted", Applicant.printEvaluate(true));
    }
}
