package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class BackendProgrammer extends Employees {

    public BackendProgrammer(String name, Double salary) {
        this.name = name;
        if (salary < 20000.0) {
            throw new IllegalArgumentException("Invalid Salary");
        }
        this.salary = salary;
        this.role = "Back End Programmer";
    }

    public double getSalary() {
        return salary;
    }
}
