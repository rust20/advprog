package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class UiUxDesigner extends Employees {

    public UiUxDesigner(String name, Double salary) {
        this.name = name;
        if (salary < 90000.0) {
            throw new IllegalArgumentException("Invalid Salary");
        }
        this.salary = salary;
        this.role = "UI/UX Designer";
    }

    public double getSalary() {
        return salary;
    }
}
