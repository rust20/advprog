package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class NetworkExpert extends Employees {

    public NetworkExpert(String name, Double salary) {
        this.name = name;
        if (salary < 50000.0) {
            throw new IllegalArgumentException("Invalid Salary");
        }
        this.salary = salary;
        this.role = "Network Expert";
    }

    public double getSalary() {
        return salary;
    }
}
