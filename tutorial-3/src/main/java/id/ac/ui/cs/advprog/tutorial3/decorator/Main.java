package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

public class Main{
    public static void main (String[] args) {
        Food burger = BreadProducer.THICK_BUN.createBreadToBeFilled();

        burger = FillingDecorator.BEEF_MEAT.addFillingToBread(burger);
        burger = FillingDecorator.CHEESE.addFillingToBread(burger);
        burger = FillingDecorator.CHILI_SAUCE.addFillingToBread(burger);
        burger = FillingDecorator.CUCUMBER.addFillingToBread(burger);
        burger = FillingDecorator.LETTUCE.addFillingToBread(burger);
        burger = FillingDecorator.TOMATO_SAUCE.addFillingToBread(burger);

        System.out.println(burger.getDescription() + ", cost: " + burger.cost());
    }
}
