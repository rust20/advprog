package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class Main {
    public static void main (String[] args) {
        Company company = new Company();
        
        company.addEmployee(new Ceo("Azka", 2000000));
        company.addEmployee(new Cto("Ali", 2000000));
        company.addEmployee(new BackendProgrammer("Faza", 2000000.0));
        company.addEmployee(new FrontendProgrammer("Gani", 2000000.0));
        company.addEmployee(new NetworkExpert("Kucing", 2000000.0));
        company.addEmployee(new SecurityExpert("Kola", 2000000.0));
        company.addEmployee(new UiUxDesigner("Potato", 2000000.0));


        System.out.println(company.getNetSalaries());

        for (Employees employee: company.getAllEmployees()) {
            System.out.printf("%s memiliki jawabatan %s dengan gaji sebesar %f",
                    employee.getName(), employee.getRole(), employee.getSalary());
        }

    }
}
