package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {

    QuackBehavior quackBehavior;
    FlyBehavior flyBehavior;

    public MallardDuck() {
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }

    public void display() {
        System.out.println("I'm a real Mallard duck");
    }
}
