import java.util.List;
class Rental {

    private Movie movie;
    private int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    public int getDaysRented() {
        return daysRented;
    }

    public double getAmount() {
        double result = 0;
        switch (this.getMovie().getPriceCode()) {
            case Movie.REGULAR:
                result += 2;
                if (this.getDaysRented() > 2)
                    result += (this.getDaysRented() - 2) * 1.5;
                break;
            case Movie.NEW_RELEASE:
                result += this.getDaysRented() * 3;
                break;
            case Movie.CHILDREN:
                result += 1.5;
                if (this.getDaysRented() > 3)
                    result += (this.getDaysRented() - 3) * 1.5;
                break;
        }
        return result;
    }

    public int calculatePoint() {
        int result = 1;
        // Add bonus for a two day new release rental
        if ((this.getMovie().getPriceCode() == Movie.NEW_RELEASE) &&
                this.getDaysRented() > 1)
            result++;
        return result;
    }

    public static double getTotalAmount(List<Rental> rentals) {
        double total = 0;
        for (Rental rental: rentals) {
            total += rental.getAmount();
        }
        return total;
    }

    public static int getFrequentRenterPoints(List<Rental> rentals) {
        int total = 0;
        for (Rental rental: rentals) {
            total += rental.calculatePoint();
        }
        return total;
    }
}
